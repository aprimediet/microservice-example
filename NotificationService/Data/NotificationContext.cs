﻿using Microsoft.EntityFrameworkCore;
using NotificationService.Models;
using System.Data.Entity.Hooks.Fluent;

namespace NotificationService.Data
{
    public class NotificationContext: DbContext
    {
        public NotificationContext(DbContextOptions<NotificationContext> options)
            : base(options)
        {
        }

        public DbSet<Notification> NotificationItems { get; set; }
    }
}
