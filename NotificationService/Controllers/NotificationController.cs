﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NotificationService.Data;
using NotificationService.Models;
using Microsoft.EntityFrameworkCore;
using NotificationService.Lib;
using NotificationService.Dtos;
using System;
using Microsoft.AspNetCore.Http;
using NotificationService.Hubs;
using Microsoft.AspNetCore.SignalR;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace NotificationService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        private readonly NotificationContext _context;
        private readonly IHubContext<NotificationHub> _notificationHubContext;

        public NotificationController(NotificationContext context, IHubContext<NotificationHub> notificationContext)
        {
            _context = context;
            _notificationHubContext = notificationContext;
        }

        // GET: /notification
        //[Authorize]
        [OneKalbeAuthorize]
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Index()
        {
            var items = await _context.NotificationItems.ToListAsync();
            var pagedResult = new PagedResult<Notification>(items, 1, 10);

            return Ok(pagedResult);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
        public async Task<ActionResult<Notification>> Create(NotificationWriteDto notificationWriteDto)
        {
            var item = new Notification
            {
                AppId = notificationWriteDto.AppId,
                Type = notificationWriteDto.Type,
                Sender = notificationWriteDto.Sender,
                Recipient = notificationWriteDto.Recipient,
                Title = notificationWriteDto.Title,
                Message = notificationWriteDto.Message,
                UIType = notificationWriteDto.UIType,
                RedirectURI = notificationWriteDto.RedirectURI,
                IsRead = false,
                CreatedDate = DateTime.Now
            };

            _context.NotificationItems.Add(item);

            await _context.SaveChangesAsync();

            //var testData = new List<string>
            //{
            //    "Satu",
            //    "Dua"
            //};
            string[] testData =
            {
                "Satu",
                "Dua"
            };


            await _notificationHubContext.Clients.All.SendAsync("SendNotificationToUser", item.Title, item.Message, testData);
            Console.WriteLine("Ngirim");

            return CreatedAtAction(nameof(GetNotification), new { id = item.Id }, item);
        }

        [OneKalbeAuthorize("superadmin", "mimin")]
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Notification>> GetNotification(long id) {
            var item = await _context.NotificationItems.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

    }
}
