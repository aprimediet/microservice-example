﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using NotificationService.Models;

namespace NotificationService.Lib
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class OneKalbeAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string[] _roles;

        public OneKalbeAuthorizeAttribute(params string[] roles)
        {
            _roles = roles;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];

            if (user == null)
            {
                context.Result = new JsonResult(new { message = "You are unauthorized!" })
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }

            if (_roles.Length > 0 && !_roles.Contains(user.Role))
            {
                context.Result = new JsonResult(new { message = "Forbidden" })
                {
                    StatusCode = StatusCodes.Status403Forbidden
                };
            }
        }
    }
}
