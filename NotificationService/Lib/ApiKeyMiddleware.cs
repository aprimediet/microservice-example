﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace NotificationService.Lib
{
    public class ApiKeyMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;

        public ApiKeyMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            var apiKey = (string)context.Request.Headers["X-API-Key"];

            if (apiKey == null || apiKey != _appSettings.ApiKey)
            {
                context.Response.StatusCode = 401;
                await context.Response.WriteAsync("You need to enter correct API Key");

                return;
            }

            await _next(context);
        }
    }
}
