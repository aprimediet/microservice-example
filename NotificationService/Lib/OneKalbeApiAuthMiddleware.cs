﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using NotificationService.Models;

namespace NotificationService.Lib
{
    public class OneKalbeApiAuthMiddleware
    {
        private readonly RequestDelegate _next;

        public OneKalbeApiAuthMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
            {
                attachUserToContext(context, token);
            }

            await _next(context);
        }

        public void attachUserToContext(HttpContext context, string token)
        {
            context.Items["User"] = new User
            {
                Email = "admin@kalbe.co.id",
                Token = token,
                Role = "superadmin",
            };
        }
    }
}
