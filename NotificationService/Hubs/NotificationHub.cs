﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace NotificationService.Hubs
{
    public class NotificationHub: Hub
    {
        private readonly ILogger _logger;

        public NotificationHub(ILogger<NotificationHub> logger) {
            _logger = logger;
        }

        public async Task SendNotificationToApp(string appId, string title, string message)
        {
            await Clients.Group(appId).SendAsync("NotificationReceive", title, message);
        }

        public async Task SendNotificationToUser(string title, string message, string[] testData)
        {
            Console.WriteLine("Sending to all: " + title);
            _logger.LogInformation("Sending to all: " + title);

            await Clients.All.SendAsync(title, message, testData);
        }
    }
}
