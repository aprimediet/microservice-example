﻿namespace NotificationService.Models
{
    public class User
    {
        public string Email { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
    }
}
