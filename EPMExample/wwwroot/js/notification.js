﻿// signalR connection

var connection;

// Notification State

var Notification = {
    unread: 0,
    items: [],
    updateState: function (key, value) {
        if (key) {
            this[key] = value;

            // Update element with data-notification-unread tags

            $("[data-notification-unread]").each(function (index, elem) {
                $(elem).text(Notification.unread);
            });

            if (key === 'items') {
                notificationDispose();
                notificationPopoverInit();
            }
        }
    }
};

// On document mounted

$(document).ready(function () {
    console.log('ready to roll');

    // Set initial state!

    Notification.updateState();

    // Initialize signalR Connection!

    signalRInit();

    // Fetch latest notification data

    notificationFetch();
});

function signalRInit() {
    connection = new signalR
        .HubConnectionBuilder()
        .withUrl("http://localhost:5002/notificationHub", {
            transport: signalR.HttpTransportType.WebSockets,
        })
        .configureLogging(signalR.LogLevel.Debug)
        .withAutomaticReconnect()
        .build();

    // SendNotificationToUser event from signalR

    connection.on("SendNotificationToUser", function (title, message, testData) {

        console.log(title, message);
        console.log(testData);

        // Add to notification state and show toast

        notificationAdd({
            title: title,
            message: message
        });
    });

    connection.start().then(function () {
        console.log('signalR Initialized!');
    });
}

// Notification update function

function notificationAdd(item) {
    Notification.updateState("items", _.concat(Notification.items, item));
    Notification.updateState("unread", Notification.unread + 1);
    notificationShow(item.title, item.message);
}

// Toast function

function notificationShow(title, message) {
    var template = Handlebars.compile($('#toast-template').html());

    // Prepend toast to make newest on the top

    $('.toast-container').prepend(template({ title: title, message: message }));

    // Show toast
    $('.toast').toast('show');

    setTimeout(() => {
        // important! Remove last added toast

        $('.toast-container .toast').last().remove();
    }, 5001);
}

// Dummy notification trigger

function notificationTrigger() {
    console.log('Triggering notification!');

    notificationAdd({
        title: "Test",
        message: "Hello World!"
    });
}

// Fetch latest notification data

function notificationFetch() {
    var res = fetch("http://localhost:5002/notification")
        .then(function (response) {

            return response.json();
        })
        .then(function (data) {
            Notification.updateState("items", _.concat(Notification.items, data.data));
            Notification.updateState("unread", data.unread);
        });
}

function notificationPopoverInit() {
    var contentTemplate = Handlebars.compile($('#notification-popover-template').html());
    var content = contentTemplate({
        notifications: _.chain(Notification.items).reverse().take(10).value(),
    })

    $("[data-notification-popover]").popover({
        trigger: 'click',
        placement: 'bottom',
        html: true,
        content: content,
    });
}

function notificationDispose() {
    $("[data-notification-popover]").popover("dispose");
}