﻿$(document).ready(function () {
    $('#trigger-form').on('submit', notificationFormSubmit);
});

function notificationFormSubmit(event) {
    var data = {
        appId: 'EPM',
        uiType: 'none',
        redirectUri: 'http://localhost:5001/notifications',
    };
    var $form = $(event.target);

    event.preventDefault();

    var inputs = $form.find("input");

    inputs.each(function (index, input) {
        var elem = $(input);
        var key = elem.attr('name');
        var value = elem.val();
        //console.log(input);
        data[key] = value;
    });

    //console.log(data);
    notificationSend(data);
}

function notificationSend(data) {
    console.log('sending data');
    fetch("http://localhost:5002/notification", {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    }).then(function (response) {
        if (response.status >= 200) {
            resetForm("#trigger-form");
        } else {
            alert("Failed to send data!");
        }
        console.log(response);
    });
}

function resetForm(className) {
    $(className)[0].reset();
}