﻿var Stateful = {
    count: 0,
    updateState: function (key, value) {
        this[key] = value;

        $("[data-text]").each(function (index, elem) {
            var tag = $(elem).attr("data-tag");

            $(elem).text(Stateful[tag]);
        });

        //console.log(this);
    }
}

$(document).ready(function () {
    Stateful.updateState();
});

$("#inc").on("click", function () {
    Stateful.updateState("count", Stateful.count + 1);
});

$("#dec").on("click", function () {
    Stateful.updateState("count", Stateful.count - 1);
});